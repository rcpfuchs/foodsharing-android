package de.foodsharing.utils

import android.widget.ImageView
import com.stfalcon.chatkit.commons.ImageLoader
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Utility class that loads images from a URL.
 */
open class ChatkitImageLoader(
    private val defaultImage: String? = null,
    private val resourceLoader: CachedResourceLoader
) : ImageLoader {

    override fun loadImage(imageView: ImageView, url: String?, payload: Any?) {
        resourceLoader.createImageObservable(url, defaultImage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    imageView.setImageBitmap(it.apply { setHasAlpha(true) })
                }, ::captureException)
    }
}
