package de.foodsharing.ui.map

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.Basket
import de.foodsharing.model.FairSharePoint
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.utils.DEFAULT_MAP_ZOOM
import de.foodsharing.utils.LOG_TAG
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.XYTileSource
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.CopyrightOverlay
import org.osmdroid.views.overlay.Marker
import java.io.File
import javax.inject.Inject

class MapFragment : Fragment(), MapContract.View, Injectable {

    private val PERMISSION_CODE = 0

    @Inject
    lateinit var presenter: MapContract.Presenter

    @Inject
    lateinit var preferences: SharedPreferences

    private lateinit var mapView: MapView
    private lateinit var markerOverlay: RadiusMarkerClusterer
    private val locationFinder: LocationFinder? by lazy { LocationFinder(context!!) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Configuration.getInstance().osmdroidBasePath = File(context?.externalCacheDir, "osmdroid")
        Configuration.getInstance().userAgentValue = "Foodsharing Android/${BuildConfig.VERSION_CODE} (it@foodsharing.network)"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        presenter.attach(this)

        val view = inflater.inflate(R.layout.fragment_map, container, false)

        // set tile map provider
        mapView = view.findViewById(R.id.map)
        mapView.setTileSource(
            XYTileSource(
                "Wikimedia", 0, 19, 256, ".png",
                arrayOf("https://maps.wikimedia.org/osm-intl/"),
                "Wikimedia Maps | Map data © OpenStreetMap contributors"
            )
        )
        // Scales tiles up according to DPI, tiles become pixelated but readable
        mapView.isTilesScaledToDpi = true

        // add default controls
        mapView.zoomController.setVisibility(CustomZoomButtonsController.Visibility.SHOW_AND_FADEOUT)
        mapView.setMultiTouchControls(true)

        // set initial zoom to last bounding box or zoom out to germany
        val boundingBox = restoreBoundingBox() ?: BoundingBox.fromGeoPoints(
                listOf(GeoPoint(46.0, 4.0), GeoPoint(55.0, 17.0)))
        mapView.addOnFirstLayoutListener { _, _, _, _, _ ->
            mapView.zoomToBoundingBox(boundingBox, false)
        }
        mapView.setZoomRounding(true)

        view.findViewById<FloatingActionButton>(R.id.map_location_button).apply {
            setOnClickListener {
                onFindLocationClick()
            }
        }

        markerOverlay = RadiusMarkerClusterer(context)
        context?.let {
            markerOverlay.setRadius((it.resources.displayMetrics.density * 100).toInt())
        }
        mapView.overlays.add(markerOverlay)

        mapView.overlays.add(CopyrightOverlay(this.context))

        presenter.fetch()

        return view
    }

    private fun findLocation() {
        locationFinder?.apply {
            initialise()
            if (isLocationAvailable())
                requestLocation { updateLocation(GeoPoint(it)) }
            else
                showLocationNotification()
        }
    }

    private fun onFindLocationClick() {
        val permission = Manifest.permission.ACCESS_COARSE_LOCATION
        if (context?.let { ContextCompat.checkSelfPermission(it, permission) } == PackageManager.PERMISSION_GRANTED) {
            findLocation()
        } else {
            // request real-time permissions (api >= 23)
            requestPermissions(
                    arrayOf(permission),
                    PERMISSION_CODE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                findLocation()
                view?.findViewById<FloatingActionButton>(R.id.map_location_button)?.show()
            } else {
                view?.findViewById<FloatingActionButton>(R.id.map_location_button)?.hide()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        storeBoundingBox()
        super.onPause()
        mapView.onPause()
    }

    private fun restoreBoundingBox(): BoundingBox? {
        if (!(preferences.contains("MAP_CENTER_LATITUDE") && preferences.contains("MAP_CENTER_LONGITUDE") && preferences.contains("MAP_CENTER_RADIUS"))) {
            return null
        }
        val centerLat = preferences.getFloat("MAP_CENTER_LATITUDE", 0.0f).toDouble()
        val centerLon = preferences.getFloat("MAP_CENTER_LONGITUDE", 0.0f).toDouble()
        val radius = preferences.getFloat("MAP_CENTER_RADIUS", 0.0f).toDouble()
        val topLeft = GeoPoint(centerLat - radius, centerLon - radius)
        val bottomRight = GeoPoint(centerLat + radius, centerLon + radius)
        return BoundingBox.fromGeoPoints(listOf(topLeft, bottomRight))
    }

    private fun storeBoundingBox() {
        val radius = 0.5 * Math.min(mapView.boundingBox.latitudeSpan, mapView.boundingBox.longitudeSpan)
        val centerLat = mapView.boundingBox.centerLatitude
        val centerLon = mapView.boundingBox.centerLongitude
        preferences.edit()
                .putFloat("MAP_CENTER_LATITUDE", centerLat.toFloat())
                .putFloat("MAP_CENTER_LONGITUDE", centerLon.toFloat())
                .putFloat("MAP_CENTER_RADIUS", radius.toFloat())
                .apply()
    }

    override fun setMarkers(fairSharePoints: List<FairSharePoint>, baskets: List<Basket>) {
        markerOverlay.items.clear()

        // add fair-share-point markers
        val fspIcon = ContextCompat.getDrawable(context!!, R.drawable.marker_fairteiler)
        fairSharePoints.map { fsp ->
            val marker = Marker(mapView)
            marker.id = fsp.id.toString()
            marker.position = GeoPoint(fsp.lat, fsp.lon)
            marker.icon = fspIcon
            marker.relatedObject = fsp
            marker.setOnMarkerClickListener { m, _ ->
                val fairsp = m.relatedObject as FairSharePoint
                Log.v(LOG_TAG, "$fairsp")
                true
            }
            marker
        }.forEach { markerOverlay.add(it) }

        val basketIcon = ContextCompat.getDrawable(context!!, R.drawable.marker_basket)
        baskets.map { b ->
            val marker = Marker(mapView)
            marker.id = b.id.toString()
            marker.position = GeoPoint(b.lat, b.lon)
            marker.icon = basketIcon
            marker.relatedObject = b
            marker.setOnMarkerClickListener { m, _ ->
                val basket = m.relatedObject as Basket
                Log.v(LOG_TAG, "$basket")

                // show basket details in an activity
                val intent = Intent(context, BasketActivity::class.java)
                intent.putExtra("id", basket.id)
                startActivity(intent)
                activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
                true
            }
            marker
        }.forEach { markerOverlay.add(it) }

        markerOverlay.invalidate()
        mapView.invalidate()
    }

    override fun onDestroy() {
        presenter.unsubscribe()
        super.onDestroy()
    }

    /**
     * Sets the location and zoom of the map.
     */
    private fun updateLocation(point: GeoPoint) {
        mapView.controller.animateTo(point, DEFAULT_MAP_ZOOM, null)
    }

    /**
     * Notifies the user that location services are disabled.
     */
    private fun showLocationNotification() {
        AlertDialog.Builder(context)
            .setMessage(R.string.map_location_services_not_enabled)
            .setPositiveButton(R.string.map_open_location_settings, object : DialogInterface.OnClickListener {
                override fun onClick(paramDialogInterface: DialogInterface, paramInt: Int) {
                    context?.startActivity(Intent(ACTION_LOCATION_SOURCE_SETTINGS))
                }
            })
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }
}
