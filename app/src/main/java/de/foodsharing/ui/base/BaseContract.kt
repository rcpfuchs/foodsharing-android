package de.foodsharing.ui.base

import android.util.Log
import de.foodsharing.utils.LOG_TAG

class BaseContract {

    interface Presenter<in T> {
        fun unsubscribe()
        fun attach(view: T)
    }

    interface View {
        fun showErrorMessage(error: String) {
            Log.e(LOG_TAG, "$error")
        }

        fun showErrorMessage(error: Throwable) {
            showErrorMessage(error.localizedMessage)
        }
    }
}