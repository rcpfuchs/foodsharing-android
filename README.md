# foodsharing android app

An android app for foodsharing. Not yet released;

* written in [Kotlin](https://kotlinlang.org/)
* uses dagger/retrofit/okhttp libraries

Current features:
* account
  * login and logout
* chat
  * list of existing chats
  * individual chat with websocket updates
* map
  * shows food baskets and fair share points

It's quite basic for now still, lots of rough edges.

## Getting Started

You need to connect to a backend, there are two options:
* beta.foodsharing.de (default if you do nothing else)
* local backend (see [main repo](https://gitlab.com/foodsharing-dev/foodsharing) for details).

To use a local backend create/edit a `local.properties` file in the base directory with the contents: 
```
foodsharing.baseURL=http://your.ip.address.here:18080
```

## Commands

There are a few useful things you might run manually, or integrate into your IDE.

| command | purpose |
|---|---|
| `./gradlew ktlintCheck` | check code according to [ktlint](https://ktlint.github.io/) |
| `./gradlew ktlintFormat` | reformat code according to [ktlint](https://ktlint.github.io/) |
| `./gradlew ktlintApplyToIdea` | generate and apply ktlint rules to Intellij IDEs using [JLLeitschuh/ktlint-gradle](https://github.com/JLLeitschuh/ktlint-gradle#additional-helper-tasks) |
| `./gradlew dependencyUpdates` | check for dependency updates using [ben-manes/gradle-versions-plugin](https://github.com/ben-manes/gradle-versions-plugin) |


## IDE

We use either Android Studio or Intellij Ultimate Edition.

You might be able to get it to work in some other way, but you'll be on your own for now :) 

## Community

Come and say hi in __#fs-dev-android__ channel in 
[yunity slack](https://slackin.yunity.org/).

## Builds

You can get access to debug builds via GitLab artifacts for a given pipeline build.

Also published to __#fs-dev-android-git__ channel in 
[yunity slack](https://slackin.yunity.org/)

## Play store releases

Releasing to play store is done as follows:
1. update the changelog to include a new release number at the top, commit this change
2. create a tag for that release, e.g. "v0.0.3" (with `git tag v0.0.3`)
3. push the change and tag with `git push --tags`
4. then go and tidy up for the next person by creating a new "[Unreleased]" section at the top of the changelog
5. that's it!

That will trigger the CI to build a release version and push it to play store.

Currently we only deploy to the _internal_ track (a specific list containing a small number of _internal_ people).

### A few more details

#### Logo, Screenshots, Feature image, description, etc.

We use fastlane to help us out with the uploading process,
there are various images and text files in `fastlane/metadata/android/de-DE` which are used for the play store entry.

Make sure they meet the android guidelines (e.g. logo is 512x512 png).
You'll have to look those things up yourself.

#### Account

There is a foodsharing play store acount connected to info@foodsharing.de.

We can give access to other people too if needed. Nick currently is the only extra other person with access.

#### Key and passwords

We use the "let play store manage my signing key" option,
but we still need to sign our apk build with a key,
this is called the "upload" key.

Play store then unpack it, and resign it with the key they manage. We never handle this key ourselves.

Then there is the keystore password, the key password, and the google service account json.

For using in CI they are stored as GitLab protected environment variables.
Master branch and all tags are marked as protected and they are accessible via builds done for thoses git refs.
Only people in the maintainers group can create/push these refs.
