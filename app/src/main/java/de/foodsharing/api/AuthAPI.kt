package de.foodsharing.api

import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Request body for a login request.
 *
 * @see AuthAPI.login
 */
data class LoginRequest(val email: String, val password: String, val rememberMe: Boolean)

/**
 * Response of a login request.
 *
 * @see AuthAPI.login
 */
data class UserResponse(val id: Int, val name: String)

/**
 * Retrofit API interface for user authentication.
 */
interface AuthAPI {
    /**
     * Fetches the current logged in user, or 404.
     * @return the user's id and name
     */
    @GET("/api/user/current")
    fun current(): Observable<UserResponse>

    /**
     * Logs in the user.
     * @param data email and password
     * @return the user's id and name
     */
    @POST("/api/user/login")
    fun login(@Body data: LoginRequest): Observable<UserResponse>

    /**
     * Logs out the user.
     */
    @GET("/xhrapp.php?app=api&m=logout")
    fun logout(): Observable<XhrResponse>
}