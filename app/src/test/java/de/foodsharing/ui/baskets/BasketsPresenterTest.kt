package de.foodsharing.ui.baskets

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.api.BasketAPI
import de.foodsharing.model.Basket
import de.foodsharing.test.configureTestSchedulers
import de.foodsharing.test.createRandomBasket
import de.foodsharing.test.createRandomUser
import io.reactivex.Observable.just
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BasketsPresenterTest {

    lateinit var presenter: BasketsPresenter

    @Mock
    lateinit var view: BasketsContract.View

    @Mock
    lateinit var api: BasketAPI

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
        presenter = BasketsPresenter(api)
        presenter.attach(view)
    }

    @Test
    fun `fetch my baskets`() {
        val creator = createRandomUser()
        val baskets = mutableListOf<Basket>()
        for (i in 1..10) {
            baskets.add(createRandomBasket(creator))
        }

        whenever(api.list()) doReturn just(BasketAPI.BasketListResponse().apply {
            this.baskets = baskets
        })
        presenter.fetch()
        verify(view).display(baskets)
    }

    @After
    fun after() {
        verifyNoMoreInteractions(view)
    }
}
