package de.foodsharing.api

import io.reactivex.Observable
import java.util.Date

interface WebsocketAPI {

    interface Message

    data class ConversationMessage(
        val id: Int,
        val cid: Int,
        val fsId: Int,
        val fsName: String,
        val fsPhoto: String?,
        val body: String,
        val time: Date
    ) : Message

    fun subscribe(): Observable<Message>
    fun close()
}