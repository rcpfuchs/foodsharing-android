package de.foodsharing.ui.map

import android.annotation.SuppressLint
import android.content.Context
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.LocationProvider
import android.os.Bundle
import de.foodsharing.utils.captureException

class LocationFinder(context: Context) {

    private var locationManager: LocationManager? =
        context.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
    private var provider: LocationProvider? = null

    fun initialise() {
        locationManager?.let { manager ->
            val providerName = manager.getBestProvider(Criteria(), true)
            if (providerName != null) {
                provider = manager.getProvider(providerName)
            }
        }
    }

    fun isLocationAvailable() = (provider != null)

    fun requestLocation(callback: (Location) -> Unit) {
        // This is only called in cases where the permission was already checked
        @SuppressLint("MissingPermission")
        val lastLocation = locationManager?.getLastKnownLocation(provider?.name)
        lastLocation?.let { callback(it) }

        val listener: LocationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                if (lastLocation == null || lastLocation.distanceTo(location) > 100) {
                    callback(location)
                }
            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
            }

            override fun onProviderEnabled(provider: String) {
            }

            override fun onProviderDisabled(provider: String) {
            }
        }

        locationManager?.apply {
            provider?.let {
                try {
                    requestSingleUpdate(it.name, listener, null)
                } catch (e: SecurityException) {
                    captureException(e)
                }
            }
        }
    }
}